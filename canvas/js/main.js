var canvas = document.getElementById('drawIt');
var ctx = canvas.getContext('2d');

ctx.fillRect(0, 0, 450, 275);
ctx.clearRect(195, 107, 60, 60);
ctx.strokeRect(210, 123, 30, 30);

ctx.clearRect(95, 7, 60, 60);
ctx.strokeRect(110, 23, 30, 30);

ctx.clearRect(295, 207, 60, 60);
ctx.strokeRect(310, 223, 30, 30);

ctx.clearRect(15, 107, 170, 60);
ctx.strokeRect(30, 123, 140, 30);

ctx.clearRect(265, 107, 170, 60);
ctx.strokeRect(280, 123, 140, 30);

ctx.clearRect(195, 107, 60, 60);
ctx.strokeRect(210, 123, 30, 30);

