//Global variables
//create variables for all DOM objects that will be part of the
//application interactions
var code, random, msg;

/*
    Make sure you end your var statements above with a semi-colon.
    Better yet, use a single statement to declare your variables
    like so:
    var code, random, msg;
*/

$(function() {
    reset8Ball();
    $('form').submit(function(e) {
        e.preventDefault();
        if($('#question').val() == '') {
            
            showAnswer('You need to ask a question');
        } else {
            getFortune();
        }
        /*
            Ideally, you would want to get a fortune only if a
            question was actually provided in the text input.
            If the question input 
            is empty, have an appropriate
            message sent to Answer to reflect that.
        */
        
    });
});
 
//Helper functions
var getRandomIndex = function(max) {
    code = Math.floor(math.random()*500);  
    /*
        Since $('#question').val() produces a string
        it will always result in a value of zero
        once it is processed in the PHP script.
        
        Make sure that "code" ends up with a random
        integer value instead. You can use a simple
        application of the Math.random() function to
        do this, but make sure you end up with some
        integer value being stored in "code"
    */
};
 
 
var showAnswer = function(msg) {
     $('#answer').html(msg).delay('700').fadeIn('fast');
    $('img').delay('700').fadeOut('fast');
};
 
var getFortune = function() {
   getRandomIndex();
   $('#magic8ball').effect("shake", {times:2,direction:"up"}, 100);
    $.ajax({
        url:'php/fortune.php',
       
        type:'post',
       
        data: {"code":code},
       
        success: function(response) {
            showAnswer(response);
        },
       
        error: function(xhr) {
            showAnswer('n i c e  o n e');
        },
    });
};
 
var reset8Ball = function() {
    /*
        The click handler should be defined in the document ready function
        and this function should focus only on what's needed to reset the
        8-ball
    */
    
    $('#magic8ball').click(function(){
        $('#magic8ball').effect("shake", {times:1,direction:"up"}, 100);
        $('#answer').html('').fadeOut('fast');
        $('img').fadeIn('fast');
    });
};